#!/usr/bin/env python
import rospy
from rosbot_msgs.msg import Sensors_rosbot
from std_msgs.msg import Bool, Float32
from sensor_msgs.msg import Range

stop_condition=Bool()
msg_sensor=Sensors_rosbot()
range_sensor=Float32()

def callback(msg):
    global range_sensor,stop_condition
    range_sensor=msg.range
    if(msg.range<0.5):
       stop_condition=True
    else:
       stop_condition=False

def main():
    global range_sensor,stop_condition
    sub = rospy.Subscriber('/range/fl',Range,callback)
    pub = rospy.Publisher('/sensor_rosbot',Sensors_rosbot,queue_size=10)
    

    rospy.init_node('subscriber_range', anonymous=True)
    rate = rospy.Rate(10) # 10hz
    while not rospy.is_shutdown():
        print 'range_sensor',range_sensor
        msg_sensor.name='Sensor fl'
        msg_sensor.sensorir=range_sensor
        msg_sensor.state=stop_condition
        pub.publish(msg_sensor)
        rate.sleep()

if __name__ == '__main__': 
    try:
        main()
    except rospy.ROSInterruptException:
        pass

