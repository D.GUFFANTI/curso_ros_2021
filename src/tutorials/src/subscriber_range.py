#!/usr/bin/env python
import rospy
from sensor_msgs.msg import Range
from std_msgs.msg import Bool 

stop_condition=Bool()

def callback(msg):
    print 'Range:',msg.range
    if(msg.range<0.5):
       stop_condition=True
    else:
       stop_condition=False

def talker():
    sub = rospy.Subscriber('/range/fl',Range,callback)
    pub = rospy.Publisher('/stop_condition',Bool,queue_size=10)
    rospy.init_node('subscriber_range', anonymous=True)
    rate = rospy.Rate(10) # 10hz
    while not rospy.is_shutdown():
        pub.publish(stop_condition)
        rate.sleep()

if __name__ == '__main__': 
    try:
        talker()
    except rospy.ROSInterruptException:
        pass

