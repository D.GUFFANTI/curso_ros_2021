// Generated by gencpp from file rosbot_msgs/Sensors_rosbot.msg
// DO NOT EDIT!


#ifndef ROSBOT_MSGS_MESSAGE_SENSORS_ROSBOT_H
#define ROSBOT_MSGS_MESSAGE_SENSORS_ROSBOT_H


#include <string>
#include <vector>
#include <map>

#include <ros/types.h>
#include <ros/serialization.h>
#include <ros/builtin_message_traits.h>
#include <ros/message_operations.h>


namespace rosbot_msgs
{
template <class ContainerAllocator>
struct Sensors_rosbot_
{
  typedef Sensors_rosbot_<ContainerAllocator> Type;

  Sensors_rosbot_()
    : name()
    , sensorir(0.0)
    , state(false)  {
    }
  Sensors_rosbot_(const ContainerAllocator& _alloc)
    : name(_alloc)
    , sensorir(0.0)
    , state(false)  {
  (void)_alloc;
    }



   typedef std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other >  _name_type;
  _name_type name;

   typedef float _sensorir_type;
  _sensorir_type sensorir;

   typedef uint8_t _state_type;
  _state_type state;





  typedef boost::shared_ptr< ::rosbot_msgs::Sensors_rosbot_<ContainerAllocator> > Ptr;
  typedef boost::shared_ptr< ::rosbot_msgs::Sensors_rosbot_<ContainerAllocator> const> ConstPtr;

}; // struct Sensors_rosbot_

typedef ::rosbot_msgs::Sensors_rosbot_<std::allocator<void> > Sensors_rosbot;

typedef boost::shared_ptr< ::rosbot_msgs::Sensors_rosbot > Sensors_rosbotPtr;
typedef boost::shared_ptr< ::rosbot_msgs::Sensors_rosbot const> Sensors_rosbotConstPtr;

// constants requiring out of line definition



template<typename ContainerAllocator>
std::ostream& operator<<(std::ostream& s, const ::rosbot_msgs::Sensors_rosbot_<ContainerAllocator> & v)
{
ros::message_operations::Printer< ::rosbot_msgs::Sensors_rosbot_<ContainerAllocator> >::stream(s, "", v);
return s;
}

} // namespace rosbot_msgs

namespace ros
{
namespace message_traits
{



// BOOLTRAITS {'IsFixedSize': False, 'IsMessage': True, 'HasHeader': False}
// {'rosbot_msgs': ['/home/ros2021/ros_examples/src/rosbot_msgs/msg'], 'std_msgs': ['/opt/ros/kinetic/share/std_msgs/cmake/../msg']}

// !!!!!!!!!!! ['__class__', '__delattr__', '__dict__', '__doc__', '__eq__', '__format__', '__getattribute__', '__hash__', '__init__', '__module__', '__ne__', '__new__', '__reduce__', '__reduce_ex__', '__repr__', '__setattr__', '__sizeof__', '__str__', '__subclasshook__', '__weakref__', '_parsed_fields', 'constants', 'fields', 'full_name', 'has_header', 'header_present', 'names', 'package', 'parsed_fields', 'short_name', 'text', 'types']




template <class ContainerAllocator>
struct IsFixedSize< ::rosbot_msgs::Sensors_rosbot_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct IsFixedSize< ::rosbot_msgs::Sensors_rosbot_<ContainerAllocator> const>
  : FalseType
  { };

template <class ContainerAllocator>
struct IsMessage< ::rosbot_msgs::Sensors_rosbot_<ContainerAllocator> >
  : TrueType
  { };

template <class ContainerAllocator>
struct IsMessage< ::rosbot_msgs::Sensors_rosbot_<ContainerAllocator> const>
  : TrueType
  { };

template <class ContainerAllocator>
struct HasHeader< ::rosbot_msgs::Sensors_rosbot_<ContainerAllocator> >
  : FalseType
  { };

template <class ContainerAllocator>
struct HasHeader< ::rosbot_msgs::Sensors_rosbot_<ContainerAllocator> const>
  : FalseType
  { };


template<class ContainerAllocator>
struct MD5Sum< ::rosbot_msgs::Sensors_rosbot_<ContainerAllocator> >
{
  static const char* value()
  {
    return "0c6530654ea7474f2ad7edea74e32164";
  }

  static const char* value(const ::rosbot_msgs::Sensors_rosbot_<ContainerAllocator>&) { return value(); }
  static const uint64_t static_value1 = 0x0c6530654ea7474fULL;
  static const uint64_t static_value2 = 0x2ad7edea74e32164ULL;
};

template<class ContainerAllocator>
struct DataType< ::rosbot_msgs::Sensors_rosbot_<ContainerAllocator> >
{
  static const char* value()
  {
    return "rosbot_msgs/Sensors_rosbot";
  }

  static const char* value(const ::rosbot_msgs::Sensors_rosbot_<ContainerAllocator>&) { return value(); }
};

template<class ContainerAllocator>
struct Definition< ::rosbot_msgs::Sensors_rosbot_<ContainerAllocator> >
{
  static const char* value()
  {
    return "string name\n\
float32 sensorir\n\
bool state \n\
";
  }

  static const char* value(const ::rosbot_msgs::Sensors_rosbot_<ContainerAllocator>&) { return value(); }
};

} // namespace message_traits
} // namespace ros

namespace ros
{
namespace serialization
{

  template<class ContainerAllocator> struct Serializer< ::rosbot_msgs::Sensors_rosbot_<ContainerAllocator> >
  {
    template<typename Stream, typename T> inline static void allInOne(Stream& stream, T m)
    {
      stream.next(m.name);
      stream.next(m.sensorir);
      stream.next(m.state);
    }

    ROS_DECLARE_ALLINONE_SERIALIZER
  }; // struct Sensors_rosbot_

} // namespace serialization
} // namespace ros

namespace ros
{
namespace message_operations
{

template<class ContainerAllocator>
struct Printer< ::rosbot_msgs::Sensors_rosbot_<ContainerAllocator> >
{
  template<typename Stream> static void stream(Stream& s, const std::string& indent, const ::rosbot_msgs::Sensors_rosbot_<ContainerAllocator>& v)
  {
    s << indent << "name: ";
    Printer<std::basic_string<char, std::char_traits<char>, typename ContainerAllocator::template rebind<char>::other > >::stream(s, indent + "  ", v.name);
    s << indent << "sensorir: ";
    Printer<float>::stream(s, indent + "  ", v.sensorir);
    s << indent << "state: ";
    Printer<uint8_t>::stream(s, indent + "  ", v.state);
  }
};

} // namespace message_operations
} // namespace ros

#endif // ROSBOT_MSGS_MESSAGE_SENSORS_ROSBOT_H
