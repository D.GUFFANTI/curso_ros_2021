
(cl:in-package :asdf)

(defsystem "custom_msg-msg"
  :depends-on (:roslisp-msg-protocol :roslisp-utils )
  :components ((:file "_package")
    (:file "sensor_rosbot" :depends-on ("_package_sensor_rosbot"))
    (:file "_package_sensor_rosbot" :depends-on ("_package"))
  ))