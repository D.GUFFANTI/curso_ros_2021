; Auto-generated. Do not edit!


(cl:in-package custom_msg-msg)


;//! \htmlinclude sensor_rosbot.msg.html

(cl:defclass <sensor_rosbot> (roslisp-msg-protocol:ros-message)
  ((name
    :reader name
    :initarg :name
    :type cl:string
    :initform "")
   (sensorir
    :reader sensorir
    :initarg :sensorir
    :type cl:float
    :initform 0.0)
   (state
    :reader state
    :initarg :state
    :type cl:boolean
    :initform cl:nil))
)

(cl:defclass sensor_rosbot (<sensor_rosbot>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <sensor_rosbot>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'sensor_rosbot)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name custom_msg-msg:<sensor_rosbot> is deprecated: use custom_msg-msg:sensor_rosbot instead.")))

(cl:ensure-generic-function 'name-val :lambda-list '(m))
(cl:defmethod name-val ((m <sensor_rosbot>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader custom_msg-msg:name-val is deprecated.  Use custom_msg-msg:name instead.")
  (name m))

(cl:ensure-generic-function 'sensorir-val :lambda-list '(m))
(cl:defmethod sensorir-val ((m <sensor_rosbot>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader custom_msg-msg:sensorir-val is deprecated.  Use custom_msg-msg:sensorir instead.")
  (sensorir m))

(cl:ensure-generic-function 'state-val :lambda-list '(m))
(cl:defmethod state-val ((m <sensor_rosbot>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader custom_msg-msg:state-val is deprecated.  Use custom_msg-msg:state instead.")
  (state m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <sensor_rosbot>) ostream)
  "Serializes a message object of type '<sensor_rosbot>"
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'name))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'name))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'sensorir))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'state) 1 0)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <sensor_rosbot>) istream)
  "Deserializes a message object of type '<sensor_rosbot>"
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'name) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'name) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'sensorir) (roslisp-utils:decode-single-float-bits bits)))
    (cl:setf (cl:slot-value msg 'state) (cl:not (cl:zerop (cl:read-byte istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<sensor_rosbot>)))
  "Returns string type for a message object of type '<sensor_rosbot>"
  "custom_msg/sensor_rosbot")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'sensor_rosbot)))
  "Returns string type for a message object of type 'sensor_rosbot"
  "custom_msg/sensor_rosbot")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<sensor_rosbot>)))
  "Returns md5sum for a message object of type '<sensor_rosbot>"
  "0c6530654ea7474f2ad7edea74e32164")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'sensor_rosbot)))
  "Returns md5sum for a message object of type 'sensor_rosbot"
  "0c6530654ea7474f2ad7edea74e32164")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<sensor_rosbot>)))
  "Returns full string definition for message of type '<sensor_rosbot>"
  (cl:format cl:nil "string name~%float32 sensorir~%bool state ~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'sensor_rosbot)))
  "Returns full string definition for message of type 'sensor_rosbot"
  (cl:format cl:nil "string name~%float32 sensorir~%bool state ~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <sensor_rosbot>))
  (cl:+ 0
     4 (cl:length (cl:slot-value msg 'name))
     4
     1
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <sensor_rosbot>))
  "Converts a ROS message object to a list"
  (cl:list 'sensor_rosbot
    (cl:cons ':name (name msg))
    (cl:cons ':sensorir (sensorir msg))
    (cl:cons ':state (state msg))
))
