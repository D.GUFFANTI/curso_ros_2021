; Auto-generated. Do not edit!


(cl:in-package rosbot_msgs-msg)


;//! \htmlinclude sensors_rosbot.msg.html

(cl:defclass <sensors_rosbot> (roslisp-msg-protocol:ros-message)
  ((name
    :reader name
    :initarg :name
    :type cl:string
    :initform "")
   (sensorir
    :reader sensorir
    :initarg :sensorir
    :type cl:float
    :initform 0.0)
   (state
    :reader state
    :initarg :state
    :type cl:boolean
    :initform cl:nil))
)

(cl:defclass sensors_rosbot (<sensors_rosbot>)
  ())

(cl:defmethod cl:initialize-instance :after ((m <sensors_rosbot>) cl:&rest args)
  (cl:declare (cl:ignorable args))
  (cl:unless (cl:typep m 'sensors_rosbot)
    (roslisp-msg-protocol:msg-deprecation-warning "using old message class name rosbot_msgs-msg:<sensors_rosbot> is deprecated: use rosbot_msgs-msg:sensors_rosbot instead.")))

(cl:ensure-generic-function 'name-val :lambda-list '(m))
(cl:defmethod name-val ((m <sensors_rosbot>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader rosbot_msgs-msg:name-val is deprecated.  Use rosbot_msgs-msg:name instead.")
  (name m))

(cl:ensure-generic-function 'sensorir-val :lambda-list '(m))
(cl:defmethod sensorir-val ((m <sensors_rosbot>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader rosbot_msgs-msg:sensorir-val is deprecated.  Use rosbot_msgs-msg:sensorir instead.")
  (sensorir m))

(cl:ensure-generic-function 'state-val :lambda-list '(m))
(cl:defmethod state-val ((m <sensors_rosbot>))
  (roslisp-msg-protocol:msg-deprecation-warning "Using old-style slot reader rosbot_msgs-msg:state-val is deprecated.  Use rosbot_msgs-msg:state instead.")
  (state m))
(cl:defmethod roslisp-msg-protocol:serialize ((msg <sensors_rosbot>) ostream)
  "Serializes a message object of type '<sensors_rosbot>"
  (cl:let ((__ros_str_len (cl:length (cl:slot-value msg 'name))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) __ros_str_len) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) __ros_str_len) ostream))
  (cl:map cl:nil #'(cl:lambda (c) (cl:write-byte (cl:char-code c) ostream)) (cl:slot-value msg 'name))
  (cl:let ((bits (roslisp-utils:encode-single-float-bits (cl:slot-value msg 'sensorir))))
    (cl:write-byte (cl:ldb (cl:byte 8 0) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 8) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 16) bits) ostream)
    (cl:write-byte (cl:ldb (cl:byte 8 24) bits) ostream))
  (cl:write-byte (cl:ldb (cl:byte 8 0) (cl:if (cl:slot-value msg 'state) 1 0)) ostream)
)
(cl:defmethod roslisp-msg-protocol:deserialize ((msg <sensors_rosbot>) istream)
  "Deserializes a message object of type '<sensors_rosbot>"
    (cl:let ((__ros_str_len 0))
      (cl:setf (cl:ldb (cl:byte 8 0) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) __ros_str_len) (cl:read-byte istream))
      (cl:setf (cl:slot-value msg 'name) (cl:make-string __ros_str_len))
      (cl:dotimes (__ros_str_idx __ros_str_len msg)
        (cl:setf (cl:char (cl:slot-value msg 'name) __ros_str_idx) (cl:code-char (cl:read-byte istream)))))
    (cl:let ((bits 0))
      (cl:setf (cl:ldb (cl:byte 8 0) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 8) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 16) bits) (cl:read-byte istream))
      (cl:setf (cl:ldb (cl:byte 8 24) bits) (cl:read-byte istream))
    (cl:setf (cl:slot-value msg 'sensorir) (roslisp-utils:decode-single-float-bits bits)))
    (cl:setf (cl:slot-value msg 'state) (cl:not (cl:zerop (cl:read-byte istream))))
  msg
)
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql '<sensors_rosbot>)))
  "Returns string type for a message object of type '<sensors_rosbot>"
  "rosbot_msgs/sensors_rosbot")
(cl:defmethod roslisp-msg-protocol:ros-datatype ((msg (cl:eql 'sensors_rosbot)))
  "Returns string type for a message object of type 'sensors_rosbot"
  "rosbot_msgs/sensors_rosbot")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql '<sensors_rosbot>)))
  "Returns md5sum for a message object of type '<sensors_rosbot>"
  "0c6530654ea7474f2ad7edea74e32164")
(cl:defmethod roslisp-msg-protocol:md5sum ((type (cl:eql 'sensors_rosbot)))
  "Returns md5sum for a message object of type 'sensors_rosbot"
  "0c6530654ea7474f2ad7edea74e32164")
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql '<sensors_rosbot>)))
  "Returns full string definition for message of type '<sensors_rosbot>"
  (cl:format cl:nil "string name~%float32 sensorir~%bool state ~%~%~%"))
(cl:defmethod roslisp-msg-protocol:message-definition ((type (cl:eql 'sensors_rosbot)))
  "Returns full string definition for message of type 'sensors_rosbot"
  (cl:format cl:nil "string name~%float32 sensorir~%bool state ~%~%~%"))
(cl:defmethod roslisp-msg-protocol:serialization-length ((msg <sensors_rosbot>))
  (cl:+ 0
     4 (cl:length (cl:slot-value msg 'name))
     4
     1
))
(cl:defmethod roslisp-msg-protocol:ros-message-to-list ((msg <sensors_rosbot>))
  "Converts a ROS message object to a list"
  (cl:list 'sensors_rosbot
    (cl:cons ':name (name msg))
    (cl:cons ':sensorir (sensorir msg))
    (cl:cons ':state (state msg))
))
