
(cl:in-package :asdf)

(defsystem "rosbot_msgs-msg"
  :depends-on (:roslisp-msg-protocol :roslisp-utils )
  :components ((:file "_package")
    (:file "Sensors_rosbot" :depends-on ("_package_Sensors_rosbot"))
    (:file "_package_Sensors_rosbot" :depends-on ("_package"))
  ))