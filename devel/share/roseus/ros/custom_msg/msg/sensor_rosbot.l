;; Auto-generated. Do not edit!


(when (boundp 'custom_msg::sensor_rosbot)
  (if (not (find-package "CUSTOM_MSG"))
    (make-package "CUSTOM_MSG"))
  (shadow 'sensor_rosbot (find-package "CUSTOM_MSG")))
(unless (find-package "CUSTOM_MSG::SENSOR_ROSBOT")
  (make-package "CUSTOM_MSG::SENSOR_ROSBOT"))

(in-package "ROS")
;;//! \htmlinclude sensor_rosbot.msg.html


(defclass custom_msg::sensor_rosbot
  :super ros::object
  :slots (_name _sensorir _state ))

(defmethod custom_msg::sensor_rosbot
  (:init
   (&key
    ((:name __name) "")
    ((:sensorir __sensorir) 0.0)
    ((:state __state) nil)
    )
   (send-super :init)
   (setq _name (string __name))
   (setq _sensorir (float __sensorir))
   (setq _state __state)
   self)
  (:name
   (&optional __name)
   (if __name (setq _name __name)) _name)
  (:sensorir
   (&optional __sensorir)
   (if __sensorir (setq _sensorir __sensorir)) _sensorir)
  (:state
   (&optional __state)
   (if __state (setq _state __state)) _state)
  (:serialization-length
   ()
   (+
    ;; string _name
    4 (length _name)
    ;; float32 _sensorir
    4
    ;; bool _state
    1
    ))
  (:serialize
   (&optional strm)
   (let ((s (if strm strm
              (make-string-output-stream (send self :serialization-length)))))
     ;; string _name
       (write-long (length _name) s) (princ _name s)
     ;; float32 _sensorir
       (sys::poke _sensorir (send s :buffer) (send s :count) :float) (incf (stream-count s) 4)
     ;; bool _state
       (if _state (write-byte -1 s) (write-byte 0 s))
     ;;
     (if (null strm) (get-output-stream-string s))))
  (:deserialize
   (buf &optional (ptr- 0))
   ;; string _name
     (let (n) (setq n (sys::peek buf ptr- :integer)) (incf ptr- 4) (setq _name (subseq buf ptr- (+ ptr- n))) (incf ptr- n))
   ;; float32 _sensorir
     (setq _sensorir (sys::peek buf ptr- :float)) (incf ptr- 4)
   ;; bool _state
     (setq _state (not (= 0 (sys::peek buf ptr- :char)))) (incf ptr- 1)
   ;;
   self)
  )

(setf (get custom_msg::sensor_rosbot :md5sum-) "0c6530654ea7474f2ad7edea74e32164")
(setf (get custom_msg::sensor_rosbot :datatype-) "custom_msg/sensor_rosbot")
(setf (get custom_msg::sensor_rosbot :definition-)
      "string name
float32 sensorir
bool state 

")



(provide :custom_msg/sensor_rosbot "0c6530654ea7474f2ad7edea74e32164")


