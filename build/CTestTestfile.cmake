# CMake generated Testfile for 
# Source directory: /home/ros2021/ros_examples/src
# Build directory: /home/ros2021/ros_examples/build
# 
# This file includes the relevant testing commands required for 
# testing this directory and lists subdirectories to be tested as well.
subdirs(gtest)
subdirs(rosbot_msgs)
subdirs(tutorials)
subdirs(rosbot_description/src/rosbot_navigation)
subdirs(rosbot_description/src/rosbot_description)
subdirs(rosbot_description/src/rosbot_gazebo)
